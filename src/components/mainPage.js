import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { notesLoadStart,loadUserStart,relogin } from "../ducks/";

import { Header, NoteListContainer,ItemAddForm } from ".";

const MainPage = () => {
  const dispatch = useDispatch();
  const loggedIn = useSelector((state) => state.authorizationReducer.loggedIn);
  useEffect(() => {
    if (localStorage.getItem("jwt_token")) {
      dispatch(relogin());
    }
    if(loggedIn){
    dispatch(notesLoadStart());
    dispatch(loadUserStart());
    }
  },[dispatch,loggedIn]);

  return (
    <Fragment>
      <Header />
      <NoteListContainer />
      <ItemAddForm/>
    </Fragment>
  );
};

export default MainPage;
