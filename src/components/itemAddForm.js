import React, { useState } from "react";
import { useDispatch } from "react-redux";

import {
  StyledItemAddForm,
  StyledItemFormTextInput,
  StyledItemFormButton,
} from "../styled/";
import { addNoteStart } from "../ducks/";
const ItemAddForm = () => {
  const dispatch = useDispatch();
  const [noteText, setNoteText] = useState("");
  const onSubmit = (event) => {
      event.preventDefault();
      dispatch(addNoteStart(noteText));
      setNoteText('');
  };
  const onLabelChange = (event) => {
    setNoteText(event.target.value);
  };
  return (
    <StyledItemAddForm onSubmit={onSubmit}>
      <StyledItemFormTextInput
        onChange={onLabelChange}
        value={noteText}
        type="text"
        placeholder="Enter new note and press 'Add note' button"
      ></StyledItemFormTextInput>
      <StyledItemFormButton className="item-form-button">
        Add Note
      </StyledItemFormButton>
    </StyledItemAddForm>
  );
};

export default ItemAddForm;
