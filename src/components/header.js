import React, { Fragment } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";

import { logout, removeAllNotes } from "../ducks/";

import { StyledLogoutButton } from "../styled";

const Header = () => {
  const dispatch = useDispatch();
  const loggedIn = useSelector((state) => state.authorizationReducer.loggedIn);
  const user = useSelector((state) => state.userReducer.user);

  const logoutFromAccount = () => {
    dispatch(logout());
    dispatch(removeAllNotes());
  };

  return (
    <nav className="navbar navbar-expand-sm navbar-light bg-light">
      <div className="navbar-nav">
        {loggedIn ? (
          <Fragment>
            <StyledLogoutButton
              onClick={logoutFromAccount}
              className="nav-item nav-link"
            >
              Logout
            </StyledLogoutButton>
            <Link className="nav-item nav-link" to={"/profile"}>{user.username}</Link>
          </Fragment>
        ) : (
          <Fragment>
            <Link className="nav-item nav-link" to={"/register"}>
              Register
            </Link>
            <Link className="nav-item nav-link" to={"/login"}>
              Login
            </Link>
          </Fragment>
        )}
      </div>
    </nav>
  );
};

export default Header;
