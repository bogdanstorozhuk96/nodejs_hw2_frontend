import React, { useEffect, useState, Fragment } from "react";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import { ErrorIndicator, Spinner } from "./";
import {
  StyledProfileContainer,
  StyledProfileButtonWrapper,
  StyledProfileH1,
  StyledProfileForm,
  StyledProfileChangePasswordP,
} from "../styled/";
import {
  notesLoadStart,
  loadUserStart,
  relogin,
  changePasswordStart,
  logout,
  removeAllNotes,
  deleteUserStart
} from "../ducks/";

const ProfilePage = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const loading = useSelector((state) => state.userReducer.loading);
  const error = useSelector((state) => state.userReducer.error);

  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");

  const user = useSelector((state) => state.userReducer.user);
  const loggedIn = useSelector((state) => state.authorizationReducer.loggedIn);

  useEffect(() => {
    if (localStorage.getItem("jwt_token")) {
      dispatch(relogin());
    }
    if (loggedIn) {
      dispatch(notesLoadStart());
      dispatch(loadUserStart());
    }
  }, [dispatch, loggedIn]);

  const changeOldPassword = (event) => {
    setOldPassword(event.target.value);
  };

  const changeNewPassword = (event) => {
    setNewPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    dispatch(changePasswordStart({ oldPassword, newPassword }));
    dispatch(logout());
    dispatch(removeAllNotes());
    history.push(`/`);
  };
  
  const handeDelete = (event) => {
      event.preventDefault();
      dispatch(deleteUserStart());
      dispatch(logout());
      dispatch(removeAllNotes());
      history.push(`/`);
  }

  let element;
  if (loading) {
    element = <Spinner />;
  } else if (error) {
    element = <ErrorIndicator />;
  } else {
    element = null;
  }

  return (
    <Fragment>
      <StyledProfileContainer className="container">
        <StyledProfileH1>Profile {user.username}</StyledProfileH1>
        <StyledProfileButtonWrapper>
          <button onClick={handeDelete} className="btn btn-danger">Delete</button>
        </StyledProfileButtonWrapper>
        <StyledProfileChangePasswordP>
          Here you can change password
        </StyledProfileChangePasswordP>
        <StyledProfileForm onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="oldPassword">Old password</label>
            <input
              type="password"
              className="form-control"
              value={oldPassword}
              onChange={changeOldPassword}
              id="oldPassword"
              placeholder="old password"
            />
          </div>
          <div className="form-group">
            <label htmlFor="newPassword">New password</label>
            <input
              type="password"
              className="form-control"
              value={newPassword}
              id="newPassword"
              placeholder="new password"
              onChange={changeNewPassword}
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </StyledProfileForm>
      </StyledProfileContainer>
      {element}
    </Fragment>
  );
};

export default ProfilePage;
