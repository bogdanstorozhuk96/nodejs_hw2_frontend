import React from "react";
import { useSelector } from "react-redux";

import { Spinner, ErrorIndicator, Note } from ".";
import { StyledList, StyledListItemContainer } from "../styled/";

const NoteList = ({ notes }) => (
  <StyledList>
    {notes.map((note) => (
      <StyledListItemContainer key={note._id}>
        <Note {...note} />
      </StyledListItemContainer>
    ))}
  </StyledList>
);

export const NoteListContainer = () => {
  const notes = useSelector((state) => state.noteReducer.notes);
  const loading = useSelector((state) => state.noteReducer.loading);
  const error = useSelector((state) => state.noteReducer.error);
  if (loading) {
    return <Spinner />;
  }

  if (error) {
    return <ErrorIndicator />;
  }
  return <NoteList notes={notes}/>;
};
export default NoteListContainer;
