import React, { useState, Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";

import { registrationStart, loginStart} from "../ducks";

import { ErrorIndicator, Spinner } from "./";

const RegisterPage = ({ url }) => {
  const dispatch = useDispatch();

  const loading = useSelector((state) => state.authorizationReducer.loading);
  const error = useSelector((state) => state.authorizationReducer.error);

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    if (url === "/register") {
      dispatch(registrationStart({ username, password }));
    } else if (url === "/login") {
      dispatch(loginStart({ username, password }));
    }
    // history.push(`/`);
  };

  const changePassword = (event) => {
    setPassword(event.target.value);
  };

  const changeUsername = (event) => {
    setUsername(event.target.value);
  };

  // if (loggedIn && url === "/login") {
  //   history.push(`/`);
  // }

  let element;
  if (loading) {
    element = <Spinner />;
  } else if (error) {
    element = <ErrorIndicator />;
  } else {
    element = null;
  }

  return (
    <Fragment>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="username">Username</label>
          <input
            type="text"
            className="form-control"
            value={username}
            onChange={changeUsername}
            id="username"
            placeholder="username"
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            className="form-control"
            value={password}
            id="password"
            placeholder="password"
            onChange={changePassword}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
      {element}
    </Fragment>
  );
};

export default RegisterPage;
