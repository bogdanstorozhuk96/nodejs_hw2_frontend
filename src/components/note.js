import React from "react";
import { useDispatch } from "react-redux";

import { StyledDeleteButton, StyledListItem } from "../styled";
import { toggleCompletedStart, deleteNoteStart } from "../ducks";

const Note = ({ text, completed, _id }) => {
  const dispatch = useDispatch();
  const toggleCompleted = () => {
    dispatch(toggleCompletedStart(_id));
  };
  const onDelete = () => {
    dispatch(deleteNoteStart(_id));
  };
  const button = completed ? (
    <StyledDeleteButton onClick={onDelete}>Delete</StyledDeleteButton>
  ) : null;
  const itemStyle = {
    color: completed ? "#696a6c" : "#419fce",
  };
  return (
    <StyledListItem>
      <span onClick={toggleCompleted} style={itemStyle}>{text}</span>
      {button}
    </StyledListItem>
  );
};

export default Note;
