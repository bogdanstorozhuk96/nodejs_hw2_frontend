import React from "react";
import { Route, Switch } from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.min.css";

import { MainPage, RegisterPage,ProfilePage } from ".";
const App = () => (
    <Switch>
        <Route path="/" component={MainPage} exact />
        <Route path="/register/" render={({match})=>{
            const {url}=match;
            return <RegisterPage url={url}/>
        }}/>
        <Route path="/login/" render={({match})=>{
            const {url}=match;
            return <RegisterPage url={url}/>
        }}/>
        <Route path="/profile/" component={ProfilePage}/>
    </Switch>
);

export default App;
