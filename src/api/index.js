export default class MyApi {
  _apiBase = "http://localhost:8080/api";

  handleErrors = (response) => {
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return response;
  };

  register = async (newUserData) => {
    const { username, password } = newUserData;
    try {
      const result = await fetch(`${this._apiBase}/auth/register`, {
        method: "POST",
        mode: "cors",
        body: JSON.stringify({
          username,
          password,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      });
      this.handleErrors(result);
    } catch (error) {
      throw error;
    }
  };

  login = async (userData) => {
    const { username, password } = userData;
    try {
      const result = await fetch(`${this._apiBase}/auth/login`, {
        method: "POST",
        mode: "cors",
        body: JSON.stringify({
          username,
          password,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  getNotes = async () => {
    try {
      const result = await fetch(`${this._apiBase}/notes`, {
        method: "GET",
        mode: "cors",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          "Authorization": `Bearer ${localStorage.getItem("jwt_token")}`
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  toggleCompletedNoteById = async (_id) => {
    try {
      const result = await fetch(`${this._apiBase}/notes/${_id}`, {
        method: "PATCH",
        mode: "cors",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          "Authorization": `Bearer ${localStorage.getItem("jwt_token")}`
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  }

  addNote = async (text) => {
    try {
      const result = await fetch(`${this._apiBase}/notes`, {
        method: "POST",
        mode: "cors",
        body: JSON.stringify({
          text,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          "Authorization": `Bearer ${localStorage.getItem("jwt_token")}`
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  }

  deleteNodeById = async (_id) => {
    try {
      const result = await fetch(`${this._apiBase}/notes/${_id}`, {
        method: "DELETE",
        mode: "cors",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          "Authorization": `Bearer ${localStorage.getItem("jwt_token")}`
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  }

  getUser = async () => {
    try {
      const result = await fetch(`${this._apiBase}/users/me`, {
        method: "GET",
        mode: "cors",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          "Authorization": `Bearer ${localStorage.getItem("jwt_token")}`
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  }

  changePassword = async (newPassword,oldPassword) => {
    try {
      const result = await fetch(`${this._apiBase}/users/me`, {
        method: "PATCH",
        mode: "cors",
        body: JSON.stringify({
          oldPassword,
          newPassword,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          "Authorization": `Bearer ${localStorage.getItem("jwt_token")}`
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  }

  deleteUser = async () => {
    try {
      const result = await fetch(`${this._apiBase}/users/me`, {
        method: "DELETE",
        mode: "cors",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          "Authorization": `Bearer ${localStorage.getItem("jwt_token")}`
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  }
}
