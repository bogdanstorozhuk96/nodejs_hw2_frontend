import { call, put, takeEvery } from "redux-saga/effects";

import MyApi from "../api";

const myApi = new MyApi();

const REMOVE_ALL_NOTES = "nodejs_hw2_frontend/notes/REMOVE_ALL_NOTES";

const LOAD_NOTES_START = "nodejs_hw2_frontend/notes/LOAD_NOTES_START";
const LOAD_NOTES_REQUEST = "nodejs_hw2_frontend/notes/LOAD_NOTES_REQUEST";
const LOAD_NOTES_SUCCESS = "nodejs_hw2_frontend/notes/LOAD_NOTES_SUCCESS";
const LOAD_NOTES_FAILURE = "nodejs_hw2_frontend/notes/LOAD_NOTES_FAILURE";

const TOGGLE_COMPLETED_START =
  "nodejs_hw2_frontend/notes/TOGGLE_COMPLETED_START";
const TOGGLE_COMPLETED_REQUEST =
  "nodejs_hw2_frontend/notes/TOGGLE_COMPLETED_REQUEST";
const TOGGLE_COMPLETED_SUCCESS =
  "nodejs_hw2_frontend/notes/TOGGLE_COMPLETED_SUCCESS";
const TOGGLE_COMPLETED_FAILURE =
  "nodejs_hw2_frontend/notes/TOGGLE_COMPLETED_FAILURE";

const ADD_NOTE_START = "nodejs_hw2_frontend/notes/ADD_NOTE_START";
const ADD_NOTE_REQUEST = "nodejs_hw2_frontend/notes/ADD_NOTE_REQUEST";
const ADD_NOTE_SUCCESS = "nodejs_hw2_frontend/notes/ADD_NOTE_SUCCESS";
const ADD_NOTE_FAILURE = "nodejs_hw2_frontend/notes/ADD_NOTE_FAILURE";

const DELETE_NOTE_START = "nodejs_hw2_frontend/notes/DELETE_NOTE_START";
const DELETE_NOTE_REQUEST = "nodejs_hw2_frontend/notes/DELETE_NOTE_REQUEST";
const DELETE_NOTE_SUCCESS = "nodejs_hw2_frontend/notes/DELETE_NOTE_SUCCESS";
const DELETE_NOTE_FAILURE = "nodejs_hw2_frontend/notes/DELETE_NOTE_FAILURE";

const initialState = {
  searchedNote: null,
  notes: [],
  loading: false,
  error: null,
};

const noteReducer = (state = initialState, action) => {
  switch (action.type) {
    case REMOVE_ALL_NOTES:
      return {
        ...state,
        notes: []
      };

    case TOGGLE_COMPLETED_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case TOGGLE_COMPLETED_SUCCESS:
      const { notes } = state;
      const noteId = action.payload;
      const noteIndex = notes.findIndex(({ _id }) => _id === noteId);
      const updatedNote = {
        ...notes[noteIndex],
        completed: !notes[noteIndex].completed,
      };
      return {
        ...state,
        notes: [
          ...notes.slice(0, noteIndex),
          updatedNote,
          ...notes.slice(noteIndex + 1),
        ],
        loading: false,
        error: null,
      };
    case TOGGLE_COMPLETED_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case LOAD_NOTES_REQUEST:
      return {
        ...state,
        notes: [],
        loading: true,
        error: null,
      };
    case LOAD_NOTES_SUCCESS:
      return {
        ...state,
        notes: action.payload,
        loading: false,
        error: null,
      };
    case LOAD_NOTES_FAILURE:
      return {
        ...state,
        notes: [],
        loading: false,
        error: action.payload,
      };

    case ADD_NOTE_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case ADD_NOTE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
      };
    case ADD_NOTE_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case DELETE_NOTE_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case DELETE_NOTE_SUCCESS:
      const index = state.notes.findIndex(({ _id }) => _id === action.payload);
      return {
        ...state,
        notes: [
          ...state.notes.slice(0, index),
          ...state.notes.slice(index + 1),
        ],
        loading: false,
        error: null,
      };
    case DELETE_NOTE_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default noteReducer;

export const removeAllNotes =()=>({
  type: REMOVE_ALL_NOTES
})

const deleteNoteSuccess = (_id) => ({
  type: DELETE_NOTE_SUCCESS,
  payload: _id,
});
const deleteNoteRequested = () => ({
  type: DELETE_NOTE_REQUEST,
});
const deleteNoteError = (error) => ({
  type: DELETE_NOTE_FAILURE,
  payload: error,
});
export const deleteNoteStart = (_id) => ({
  type: DELETE_NOTE_START,
  payload: _id,
});

const toggleCompletedSuccess = (_id) => ({
  type: TOGGLE_COMPLETED_SUCCESS,
  payload: _id,
});
const toggleCompletedRequested = () => ({
  type: TOGGLE_COMPLETED_REQUEST,
});
const toggleCompletedError = (error) => ({
  type: TOGGLE_COMPLETED_FAILURE,
  payload: error,
});
export const toggleCompletedStart = (_id) => ({
  type: TOGGLE_COMPLETED_START,
  payload: _id,
});

const notesLoadSuccess = (newNotes) => ({
  type: LOAD_NOTES_SUCCESS,
  payload: newNotes,
});
const notesLoadRequested = () => ({
  type: LOAD_NOTES_REQUEST,
});
const notesLoadError = (error) => ({
  type: LOAD_NOTES_FAILURE,
  payload: error,
});
export const notesLoadStart = () => ({
  type: LOAD_NOTES_START,
});

const addNoteSuccess = () => ({
  type: ADD_NOTE_SUCCESS,
});
const addNoteRequested = () => ({
  type: ADD_NOTE_REQUEST,
});
const addNoteError = (error) => ({
  type: ADD_NOTE_FAILURE,
  payload: error,
});
export const addNoteStart = (text) => ({
  type: ADD_NOTE_START,
  payload: text,
});



function* deleteNoteAsync(action) {
  try {
    const _id = action.payload;
    yield put(deleteNoteRequested());
    yield call(() => myApi.deleteNodeById(_id));
    yield put(deleteNoteSuccess(_id));
  } catch (error) {
    yield put(deleteNoteError(error));
  }
}

export function* watchDeleteNote() {
  yield takeEvery(DELETE_NOTE_START, deleteNoteAsync);
}






function* addNoteAsync(action) {
  try {
    const text = action.payload;
    yield put(addNoteRequested());
    yield call(() => myApi.addNote(text));
    yield put(addNoteSuccess());
    yield put(notesLoadStart());
  } catch (error) {
    yield put(addNoteError(error));
  }
}

export function* watchAddNote() {
  yield takeEvery(ADD_NOTE_START, addNoteAsync);
}

function* toggleCompletedAsync(action) {
  try {
    const _id = action.payload;
    yield put(toggleCompletedRequested());
    yield call(() => myApi.toggleCompletedNoteById(_id));
    yield put(toggleCompletedSuccess(_id));
  } catch (error) {
    yield put(toggleCompletedError(error));
  }
}

export function* watchToggleCompleted() {
  yield takeEvery(TOGGLE_COMPLETED_START, toggleCompletedAsync);
}

function* fetchNotesAsync(action) {
  try {
    yield put(notesLoadRequested());
    const data = yield call(() => myApi.getNotes());
    yield put(notesLoadSuccess(data.notes));
  } catch (error) {
    yield put(notesLoadError(error));
  }
}

export function* watchFetchNotes() {
  yield takeEvery(LOAD_NOTES_START, fetchNotesAsync);
}
