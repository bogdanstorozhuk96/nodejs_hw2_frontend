import { combineReducers } from "redux";
import { all } from "redux-saga/effects";

import userReducer, {
  loadUserStart,
  changePasswordStart,
  deleteUserStart,
  watchLoadUser,
  watchChangePassword,
  watchDeleteUser
} from "./userReducer";

import noteReducer, {
  removeAllNotes,
  notesLoadStart,
  toggleCompletedStart,
  addNoteStart,
  deleteNoteStart,
  watchFetchNotes,
  watchToggleCompleted,
  watchAddNote,
  watchDeleteNote,
} from "./noteReducer";
import authorizationReducer, {
  registrationStart,
  loginStart,
  logout,
  relogin,
  watchRegistration,
  watchLogin,
} from "./authorizationReducer";

export default combineReducers({
  userReducer,
  noteReducer,
  authorizationReducer,
});

export {
  removeAllNotes,
  registrationStart,
  loginStart,
  logout,
  relogin,
  notesLoadStart,
  toggleCompletedStart,
  addNoteStart,
  deleteNoteStart,
  loadUserStart,
  changePasswordStart,
  deleteUserStart
};

export function* rootSaga() {
  yield all([
    watchRegistration(),
    watchLogin(),
    watchFetchNotes(),
    watchToggleCompleted(),
    watchAddNote(),
    watchDeleteNote(),
    watchLoadUser(),
    watchChangePassword(),
    watchDeleteUser()
  ]);
}
