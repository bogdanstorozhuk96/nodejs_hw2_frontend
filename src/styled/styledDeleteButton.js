import styled from "styled-components";

export default styled.button`
  background: #696a6c;
  color: white;
  font-size: 1.3rem;
  margin-left: 1rem;
  border-radius: 0.3rem;
  border: none;
  :hover {
    cursor: pointer;
  }
`;
