import styled from "styled-components";

export default styled.input`
  flex-grow: 1;
  font-size: 1.5rem;
`;
