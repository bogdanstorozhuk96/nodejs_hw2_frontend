import styled from "styled-components";

export default styled.ul`
  list-style-type: none;
  font-size: 1.5rem;
  border: 1px black solid;
  height: 400px;
  padding-left: 0rem;
  overflow-y: scroll;
`;
