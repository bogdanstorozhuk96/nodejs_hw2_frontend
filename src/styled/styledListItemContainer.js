import styled from "styled-components";

export default styled.li`
  border-bottom: 1px solid black;
  padding: 1rem 0 0 1rem;
`;
