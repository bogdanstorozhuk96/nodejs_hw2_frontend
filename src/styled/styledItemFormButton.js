import styled from "styled-components";

export default styled.button`
  font-size: 1.5rem;
  :hover {
    cursor: pointer;
  }
`;
